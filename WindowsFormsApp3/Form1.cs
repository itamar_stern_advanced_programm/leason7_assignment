﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Resources;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp3
{
    public partial class Form1 : Form
    {

        private int times = 0;
        private int lastImg = 0;

        private string msg = "0";
        private TcpClient client;
        private IPEndPoint serverEndPoint;
        private NetworkStream clientStream;

        private int _myScore = 0;
        private int _opScore = 0;

        public Form1()
        {
            InitializeComponent();
            this.BackColor = System.Drawing.Color.Lime;
            generateCards();

            for (int i = 0; i < 10; i++) this.Controls["pic" + i].Enabled = false;

            Thread myThread = new Thread(waitForMessage);
            myThread.IsBackground = true;
            myThread.Start();
        }

        private void waitForMessage()
        {

            this.client = new TcpClient();
            this.serverEndPoint = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            this.client.Connect(serverEndPoint);
            this.clientStream = client.GetStream();
            byte[] buffer = new byte[4096];
            int bytesRead = clientStream.Read(buffer, 0, 4096);

            Invoke((MethodInvoker)delegate { for (int i = 0; i < 10; i++) this.Controls["pic" + i].Enabled = true; });

            string input = "";
            while (!input.Equals("2000"))
            {

                if (!msg.Equals("0"))
                {

                    byte[] newBuffer = new ASCIIEncoding().GetBytes(msg);
                    clientStream.Write(newBuffer, 0, 4);
                    clientStream.Flush();
                    try
                    {

                        byte[] bufferIn = new byte[4];
                        bytesRead = clientStream.Read(bufferIn, 0, 4);
                        input = new ASCIIEncoding().GetString(bufferIn);
                    }
                    catch (Exception)
                    {

                        //Invoke((MethodInvoker)delegate { this.Close(); });
                    }

                    if (input[0] == '1')
                    {

                        setEnemyImage(input);

                        if (msg[0] == '1')
                        {

                            if (!msg.Substring(1, 2).Equals(input.Substring(1, 2)))
                            {

                                _myScore += (Int32.Parse(msg.Substring(1, 2)) - Int32.Parse(input.Substring(1, 2))) / Math.Abs(Int32.Parse(msg.Substring(1, 2)) - Int32.Parse(input.Substring(1, 2)));
                                _opScore += (Int32.Parse(input.Substring(1, 2)) - Int32.Parse(msg.Substring(1, 2))) / Math.Abs(Int32.Parse(input.Substring(1, 2)) - Int32.Parse(msg.Substring(1, 2)));

                                Invoke((MethodInvoker)delegate { this.Controls["myScoreText"].Text = "your score: " + _myScore.ToString(); });
                                Invoke((MethodInvoker)delegate { this.Controls["opScoreText"].Text = "opponent score: " + _opScore.ToString(); });
                            }
                        }
                    }

                    msg = "0";

                }

                Thread.Sleep(100);
            }

            close();
        }

        private void generateCards()
        {

            PictureBox EnemyImg = new PictureBox();
            EnemyImg.Name = "EnemyImg";
            EnemyImg.Image = WindowsFormsApp3.Properties.Resources.card_back_blue as Image;
            EnemyImg.Size = new Size(75, 150);
            EnemyImg.Location = new Point(Width / 2 - EnemyImg.Width / 2, EnemyImg.Height / 4);
            EnemyImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.Controls.Add(EnemyImg);

            TextBox myScore = new TextBox();
            myScore.Name = "myScoreText";
            myScore.Text = "your score: ";
            myScore.Size = new Size(100, 30);
            myScore.Location = new Point(EnemyImg.Location.X / 2 - EnemyImg.Width, 10);
            myScore.TabIndex = 10;
            myScore.TabStop = false;
            myScore.BackColor = System.Drawing.Color.Lime;
            myScore.ReadOnly = true;
            myScore.BorderStyle = BorderStyle.None;
            myScore.Font = new Font(myScore.Font.FontFamily, 10);
            this.Controls.Add(myScore);

            TextBox opScore = new TextBox();
            opScore.Name = "opScoreText";
            opScore.Text = "opponent score: ";
            opScore.Size = new Size(100, 30);
            opScore.Location = new Point(EnemyImg.Location.X * 3 / 2 + EnemyImg.Width, 10);
            opScore.TabIndex = 10;
            opScore.TabStop = false;
            opScore.BackColor = System.Drawing.Color.Lime;
            opScore.ReadOnly = true;
            opScore.BorderStyle = BorderStyle.None;
            opScore.Font = new Font(opScore.Font.FontFamily, 10);
            this.Controls.Add(opScore);

            Button Forfeit = new Button();
            Forfeit.Name = "Forfeit";
            Forfeit.Text = "Forfeit";
            Forfeit.Size = new Size(75, 30);
            Forfeit.Location = new Point(Width / 2 - EnemyImg.Width / 2, EnemyImg.Height / 4 - 40);
            Forfeit.BackColor = System.Drawing.SystemColors.Control;

            Forfeit.Click += delegate (object sender, EventArgs e)
            {

                this.Close();
            };

            this.Controls.Add(Forfeit);

            /////////////////
            Point nextL = new Point(30, Height - 200);

            for (int i = 0; i < 10; i++)
            {
                PictureBox pic = new PictureBox();
                pic.Name = "pic" + i;

                pic.Image = WindowsFormsApp3.Properties.Resources.card_back_red as Image;
                pic.Location = nextL;
                pic.Size = new Size(75, 150);
                pic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                pic.Click += delegate (object sender, EventArgs e)
                {

                    Invoke((MethodInvoker)delegate {((PictureBox)(this.Controls["pic" + lastImg.ToString()])).Image = WindowsFormsApp3.Properties.Resources.card_back_red as Image; });
                    Invoke((MethodInvoker)delegate { this.Controls["pic" + lastImg.ToString()].Enabled = true; });
                    
                    lastImg = Int32.Parse(pic.Name[3].ToString());

                    string[] cards = { "_2_of", "_3_of", "_4_of", "_5_of", "_6_of", "_7_of", "_8_of", "_9_of", "_10_of", "jack_of", "queen_of", "king_of", "ace_of" };
                    string[] types = { "_clubs", "_diamonds", "_hearts", "_spades" };
                    List<string> names = new List<string>();

                    for (int ii = 0; ii < cards.Length; ii++)
                    {

                        for (int jj = 0; jj < types.Length; jj++)
                        {

                            names.Add(cards[ii] + types[jj]);

                        }
                    }

                    EnemyImg.Image = WindowsFormsApp3.Properties.Resources.card_back_blue as Image;
                    ResourceManager rm = WindowsFormsApp3.Properties.Resources.ResourceManager;
                    int rand = new Random().Next(0, names.Count);
                    string randName = names[rand];
                    Image img = rm.GetObject(randName) as Image;

                    ((PictureBox)sender).Image = img;

                    string s = "";

                    if (randName[0] != '_') s = randName.Substring(0, randName.IndexOf('_'));
                    else s = randName.Substring(1).Substring(0, randName.Substring(1).IndexOf('_'));

                    if (s.Equals("ace")) s = "01";
                    if (s.Equals("jack")) s = "11";
                    if (s.Equals("queen")) s = "12";
                    if (s.Equals("king")) s = "13";
                    if (s.Length == 1) s = "0" + s;

                    msg = "1" + s + (char)('A' + randName.Substring(randName.LastIndexOf('_') + 1)[0] - 'a');
                    ((PictureBox)sender).Enabled = false;
                };

                this.Controls.Add(pic);

                nextL.X += pic.Width + 10;
                if (nextL.X > this.Width)
                {

                    nextL.X = 10;
                    nextL.Y += pic.Height + 10;
                }
            }
        }

        private void setEnemyImage(string s)
        {

            string[] cards = { "ace_of", "_2_of", "_3_of", "_4_of", "_5_of", "_6_of", "_7_of", "_8_of", "_9_of", "_10_of", "jack_of", "queen_of", "king_of" };
            string[] types = { "_clubs", "_diamonds", "_hearts", "_spades" };

            string value = cards[Int32.Parse(s.Substring(1, 2)) - 1];
            string type = "";

            for (int i = 0; i < types.Length; i++)
            {

                if (('A' + types[i][1] - 'a') == s[3]) type = types[i];
            }

            foreach (Control item in this.Controls)
            {

                if (item is PictureBox)
                {

                    if (item.Name.Equals("EnemyImg"))
                    {

                        ResourceManager rm = WindowsFormsApp3.Properties.Resources.ResourceManager;
                        Image img = rm.GetObject(value + type) as Image;

                        Invoke((MethodInvoker)delegate { ((PictureBox)item).Image = img; });
                    }
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            close();
        }

        private void close()
        {

            byte[] newBuffer = new ASCIIEncoding().GetBytes("2000");
            clientStream.Write(newBuffer, 0, 4);
            clientStream.Flush();

            clientStream.Close();
            msg = "aaaa";

            Invoke((MethodInvoker)delegate { this.Close(); });
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {

            if (times == 0)
            {

                exitScreen s = new exitScreen(_myScore, _opScore);
                s.ShowDialog();

                times = 1;
            }

            if (!msg.Equals("aaaa"))
            {
                
                close();
            }
        }
    }
}
