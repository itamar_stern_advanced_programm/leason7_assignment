﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace WindowsFormsApp3
{
    public partial class exitScreen : Form
    {

        public exitScreen(int myScore, int opScore)
        {
            InitializeComponent();

            this.textBox1.Font = new Font(textBox1.Font.FontFamily, 10);
            this.textBox2.Font = new Font(textBox2.Font.FontFamily, 10);

            this.textBox1.Text += myScore.ToString();
            this.textBox2.Text += opScore.ToString();
        }
        private void exitScreen_Load(object sender, EventArgs e)
        {

            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.FormBorderStyle = FormBorderStyle.FixedSingle;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
